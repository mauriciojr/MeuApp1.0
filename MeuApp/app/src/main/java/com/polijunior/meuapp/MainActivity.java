package com.polijunior.meuapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private Button bt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        bt = (Button)findViewById(R.id.btDois);
        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mostrar(v);
            }
        });
    }

    public void cadastrar(View v){
        startActivityForResult(new Intent(this, Cadastrar.class), 1);
    }

    public void mostrar(View v){
        startActivityForResult(new Intent(this, Visualizar.class), 1);
    }

}
