package com.polijunior.meuapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


public class Cadastrar extends AppCompatActivity {

   public Button btCadastrar;
    EditText nome;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cadastro);

        btCadastrar = (Button)findViewById(R.id.btCadastrar);
        nome = (EditText)findViewById(R.id.edNome);

        btCadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CharSequence n = nome.getText().toString();
                /*nome.showContextMenu();*/
                Toast t = Toast.makeText(v.getContext(), n, Toast.LENGTH_LONG);
                t.show();
                finish();

                Intent detalhe = new Intent(v.getContext(), Visualizar.class);
                Bundle arquivo = new Bundle();

                String str = nome.getText().toString();
                arquivo.putString("mensagem", str);
                detalhe.putExtras(arquivo);
                startActivity(detalhe);
            }
        });

    }
}
           /* public EditText[] nome= new EditText[12];*/

       /* nome[0] = (EditText) findViewById(R.id.edNome);
        nome[1] = (EditText) findViewById(R.id.edCpf);
        nome[2] = (EditText) findViewById(R.id.edIdade);
        nome[3] = (EditText) findViewById(R.id.edData);
        nome[4] = (EditText) findViewById(R.id.edEndereco);
        nome[5] = (EditText) findViewById(R.id.edNomeMae);
        nome[6] = (EditText) findViewById(R.id.edNomePai);
        nome[7] = (EditText) findViewById(R.id.edAlergias);
        nome[8] = (EditText) findViewById(R.id.edDoencas);
        nome[9] = (EditText) findViewById(R.id.edSintomas);
        nome[10] = (EditText) findViewById(R.id.edClinicas);
        nome[11] = (EditText) findViewById(R.id.edHistorico);  */

