package com.polijunior.meuapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by polijunior on 28/07/2016.
 */
public class Visualizar extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detalhes);

        TextView str = new TextView(this);
        Intent detalhe = getIntent();

        Bundle arquivo = detalhe.getExtras();

        if (arquivo!=null){
            String mostraNome = arquivo.getString("mensagem");
            str.setText(mostraNome);
            setContentView(str);
        }
    }

    public void retorno(View v){
        finish();
    }
}
